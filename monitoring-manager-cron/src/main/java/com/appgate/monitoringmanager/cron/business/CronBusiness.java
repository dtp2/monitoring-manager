package com.appgate.monitoringmanager.cron.business;

import com.appgate.monitoringmanager.common.dto.MonitoringRequest;
import com.appgate.monitoringmanager.common.enums.MonitoringStatus;
import com.appgate.monitoringmanager.cron.kafka.KafkaDtpProducer;
import com.appgate.monitoringmanager.persistence.dao.MonitoringConfigDao;
import com.appgate.monitoringmanager.persistence.entity.MonitoringConfig;
import com.appgate.monitoringmanager.persistence.mapper.ConfigRequestMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

@Component
@Slf4j
public class CronBusiness {
    @Value("${monitoring.frequency.minutes}")
    private int monitoringFrequencyMinutes;
    private MonitoringConfigDao monitoringConfigDao;
    private KafkaDtpProducer kafkaDtpProducer;

    public CronBusiness(MonitoringConfigDao monitoringConfigDao, KafkaDtpProducer kafkaDtpProducer) {
        this.monitoringConfigDao = monitoringConfigDao;
        this.kafkaDtpProducer = kafkaDtpProducer;
    }

    public void execute() {
        log.info(new Exception().getStackTrace()[0].getMethodName());
        List<MonitoringConfig> monitoringRecords = retrieveMonitoringRecords();
        enqueueRecords(monitoringRecords);
    }

    private void enqueueRecords(List<MonitoringConfig> monitoringRecords) {
        log.info(new Exception().getStackTrace()[0].getMethodName());
        MonitoringRequest monitoringRequest;
        for (MonitoringConfig monitoringConfig :
                monitoringRecords) {
            monitoringRequest = ConfigRequestMapper.mapper(monitoringConfig);
            monitoringRequest.put("traceUUID", UUID.randomUUID().toString());
            this.kafkaDtpProducer.enqueue(monitoringRequest);
        }
    }

    private List<MonitoringConfig> retrieveMonitoringRecords() {
        log.info(new Exception().getStackTrace()[0].getMethodName());
        ZonedDateTime now = ZonedDateTime.now();
        long limitDate = now.toEpochSecond();
        String status = MonitoringStatus.MONITORING.getStatus();
        int frequency = this.monitoringFrequencyMinutes;
        log.info("[status:{}][frequency in min: {}][Date Greater Than:{}|{}]", status, frequency, now, limitDate);
        List<MonitoringConfig> records = this.monitoringConfigDao.findByMonitoringFinalDateGreaterThanAndFrequencyAndStatus(limitDate, frequency, status);
        log.info("records: {}", records.size());
        return records;
    }

}
