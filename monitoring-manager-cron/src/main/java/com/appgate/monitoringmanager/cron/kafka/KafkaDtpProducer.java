package com.appgate.monitoringmanager.cron.kafka;

import com.appgate.monitoringmanager.common.parser.JsonParse;
import com.appgate.monitoringmanager.common.dto.MonitoringRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Component
@Slf4j
public class KafkaDtpProducer {

    private KafkaTemplate<String, String> kafkaTemplate;
    @Value("${dtp.monitoring.validator.kafka.topic}")
    private String topic;

    public KafkaDtpProducer(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void enqueue(MonitoringRequest monitoringRequest) {
        log.info("{} -> message in topic [{}]", new Exception().getStackTrace()[0].getMethodName(), this.topic);
        String message = JsonParse.parseStringData(monitoringRequest);
        ListenableFuture<SendResult<String, String>> future = this.kafkaTemplate.send(this.topic, message);
        future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
            @Override
            public void onFailure(Throwable throwable) {
                log.error("{} Unable to send message=["
                        + JsonParse.parsePrettyStringData(monitoringRequest) + "] due to : " + throwable.getMessage(), new Exception().getStackTrace()[0].getMethodName());
            }

            @Override
            public void onSuccess(SendResult<String, String> result) {
                log.info("{} Sent message", new Exception().getStackTrace()[0].getMethodName());
            }
        });
    }
}
