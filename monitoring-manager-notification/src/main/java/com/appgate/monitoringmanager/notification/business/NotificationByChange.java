package com.appgate.monitoringmanager.notification.business;

import com.appgate.monitoringmanager.common.dto.EventManagerMessage;
import com.appgate.monitoringmanager.common.dto.MonitoringRequest;
import com.appgate.monitoringmanager.common.parser.JsonParse;
import com.appgate.monitoringmanager.persistence.dao.MonitoringConfigDao;
import com.appgate.monitoringmanager.persistence.entity.MonitoringConfig;
import com.appgate.monitoringmanager.persistence.mapper.ConfigRequestMapper;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class NotificationByChange {
    private MonitoringConfigDao monitoringConfigDao;

    public NotificationByChange(MonitoringConfigDao monitoringConfigDao) {
        this.monitoringConfigDao = monitoringConfigDao;
    }

    public void execute(EventManagerMessage eventManagerMessage) {
        log.info(new Exception().getStackTrace()[0].getMethodName());
        if (eventManagerMessage.isChange()) {
            updateRecord(eventManagerMessage);
        }
    }

    private void updateRecord(EventManagerMessage eventManagerMessage) {
        log.info(new Exception().getStackTrace()[0].getMethodName());
        JsonNode monitoringRequestJsonNode = JsonParse.parseToJsonNode(eventManagerMessage.getRequestMessage());
        MonitoringRequest monitoringRequest = JsonParse.parseJsonToObject(monitoringRequestJsonNode.traverse(), MonitoringRequest.class);
        MonitoringConfig monitoringConfig = ConfigRequestMapper.mapper(monitoringRequest);
        MonitoringConfig monitoringConfigRecord = this.monitoringConfigDao.findByMonitoringId(monitoringConfig.getMonitoringId());
        monitoringConfigRecord.setCheckPointChanged(true);
        this.monitoringConfigDao.save(monitoringConfigRecord);
    }
}
