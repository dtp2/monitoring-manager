package com.appgate.monitoringmanager.notification.kafka;

import com.appgate.monitoringmanager.common.dto.EventManagerMessage;
import com.appgate.monitoringmanager.common.parser.JsonParse;
import com.appgate.monitoringmanager.notification.business.NotificationByChange;
import lombok.extern.log4j.Log4j2;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class KafkaDtpConsumer {
    private NotificationByChange notificationByChange;

    public KafkaDtpConsumer(NotificationByChange notificationByChange) {
        this.notificationByChange = notificationByChange;
    }

    @KafkaListener(topics = "${validation.event.monitor.kafka.topic}", groupId = "${validation.event.monitor.kafka.groupid}")
    public boolean listenIncidentsForCheckStatusChange(String message) {
        log.info(new Exception().getStackTrace()[0].getMethodName());
        EventManagerMessage eventManagerMessage = JsonParse.parseJsonToObject(message, EventManagerMessage.class);
        try {
            if (eventManagerMessage != null) {
                notificationByChange.execute(eventManagerMessage);
            }
        } catch (Exception e) {
            log.error("Message wasn't process", e);
        }
        return true;
    }
}
