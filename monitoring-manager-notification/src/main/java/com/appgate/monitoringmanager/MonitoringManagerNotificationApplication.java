package com.appgate.monitoringmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.appgate.monitoringmanager")
public class MonitoringManagerNotificationApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonitoringManagerNotificationApplication.class, args);
	}

}
