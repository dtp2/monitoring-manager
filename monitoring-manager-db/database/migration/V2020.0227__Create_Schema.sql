DROP TABLE IF EXISTS monitoring_config;

CREATE TABLE monitoring_config (
    id                      BIGSERIAL       PRIMARY KEY,
    monitoring_id           VARCHAR         NOT NULL,
    url                     VARCHAR         NOT NULL,
    monitoring_final_date   BIGINT          NOT NULL,
    frequency               INT             NOT NULL,
    check_point_changed     VARCHAR,
    source_type             VARCHAR         NOT NULL,
    extra_data              JSON,
    status                  VARCHAR         NOT NULL
);

DROP TABLE IF EXISTS monitoring_company;

CREATE TABLE monitoring_company (
    id                      BIGSERIAL       PRIMARY KEY,
    uuid                    VARCHAR         NOT NULL,
    name                    VARCHAR         NOT NULL,
    countryCode             VARCHAR         NOT NULL,
    enabled                 BOOLEAN         NOT NULL
);