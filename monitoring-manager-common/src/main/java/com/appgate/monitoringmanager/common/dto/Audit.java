package com.appgate.monitoringmanager.common.dto;

import lombok.Getter;
import lombok.Setter;

public class Audit {
    @Getter
    @Setter
    private String lastAuthorId;
    @Getter
    @Setter
    private String lastAuthorName;
    @Getter
    @Setter
    private Long lastModifiedDate;
    @Getter
    @Setter
    private Long createOn;
    @Getter
    @Setter
    private Boolean enabled;

}
