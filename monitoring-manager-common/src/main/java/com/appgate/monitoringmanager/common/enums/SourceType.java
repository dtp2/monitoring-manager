package com.appgate.monitoringmanager.common.enums;

public enum SourceType {
    INCIDENT_MONITORING("incidentMonitoring"),
    WHO_IS_MONITORING("whoisMonitoring"),
    WEB_SIDE_DEFACEMENT("webSideDefacement"),
    STATUS_MONITORING("statusMonitoring");
    private String type;

    SourceType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public static boolean isValidType(String type) {
        for (SourceType sourceType : SourceType.values()) {
            if (sourceType.getType().equalsIgnoreCase(type)) {
                return true;
            }
        }
        return false;
    }

    public static SourceType findByType(String type) {
        for (SourceType sourceType : SourceType.values()) {
            if (sourceType.getType().equalsIgnoreCase(type)) {
                return sourceType;
            }
        }
        return null;
    }
}
