package com.appgate.monitoringmanager.common.dto;

import lombok.Getter;
import lombok.Setter;

public class Author {
    @Getter
    @Setter
    private Long id;
    @Getter
    @Setter
    private String name;
}
