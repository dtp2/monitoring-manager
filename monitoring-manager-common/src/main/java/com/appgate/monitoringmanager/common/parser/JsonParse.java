package com.appgate.monitoringmanager.common.parser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class JsonParse {
    private static ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    private JsonParse() {
    }

    public static String parseStringData(Object data) {
        try {
            return mapper.writeValueAsString(data);
        } catch (JsonProcessingException e) {
            return "{}";
        }
    }

    public static <T> T parseJsonToObject(String stringJson, Class<T> classType) {
        try {
            return mapper.readValue(stringJson, classType);
        } catch (IOException e) {
            return null;
        }
    }

    public static String parsePrettyStringData(Object data) {
        try {
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(data);
        } catch (JsonProcessingException e) {
            return "{}";
        }
    }

    public static JsonNode parseToJsonNode(Object obj) {
        return mapper.valueToTree(obj);
    }

    public static <T> T parseJsonToObject(JsonParser traverse, Class<T> classType) {
        try {
            return mapper.readValue(traverse, classType);
        } catch (IOException e) {
            return null;
        }
    }

    public static  String parseObjectToJson(Object object){
        try {
            return mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            return null;
        }
    }
}
