package com.appgate.monitoringmanager.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;

@JsonPropertyOrder({"isChange", "requestMessage", "validationDate", "validationType", "data", "message"})
public class EventManagerMessage {
    @JsonProperty("isChange")
    private boolean isChange;
    @Setter
    @Getter
    private Object requestMessage;
    @Setter
    @Getter
    private Object validationDate;
    @Setter
    @Getter
    private String validationType;
    @Setter
    @Getter
    private Object data;
    @Setter
    @Getter
    private String message;

    @JsonProperty("isChange")
    public boolean isChange() {
        return isChange;
    }

    @JsonProperty("isChange")
    public void setChange(boolean change) {
        isChange = change;
    }
}
