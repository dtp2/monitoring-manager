package com.appgate.monitoringmanager.common.dto;

import lombok.Getter;
import lombok.Setter;

public class InputIncidentData {

    @Getter
    @Setter
    private String company;

    @Getter
    @Setter
    private int companyId;

    @Getter
    @Setter
    private String monitoringFinalDate;

    @Getter
    @Setter
    private int monitoringFrecuency;

    @Getter
    @Setter
    private String monitoringId;

    @Getter
    @Setter
    private String sourceType;

    @Getter
    @Setter
    private String status;

    @Getter
    @Setter
    private String url;
}
