package com.appgate.monitoringmanager.common.dto;

import lombok.Getter;
import lombok.Setter;

public class Company extends Audit {
    @Getter
    @Setter
    private Long id;
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private String uid;
    @Getter
    @Setter
    private Country country;
}
