package com.appgate.monitoringmanager.common.dto;

import lombok.Getter;
import lombok.Setter;

public class Country extends Audit{
    @Getter
    @Setter
    private String code;
    @Getter
    @Setter
    private String name;
}
