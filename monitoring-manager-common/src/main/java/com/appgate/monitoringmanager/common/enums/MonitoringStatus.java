package com.appgate.monitoringmanager.common.enums;

import lombok.Getter;

public enum MonitoringStatus {
    MONITORING("MONITORING"),
    RESOLVED("RESOLVED");

    @Getter
    private String status;

    MonitoringStatus(String status) {
        this.status = status;
    }
}
