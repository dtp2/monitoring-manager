package com.appgate.monitoringmanager.common.dto;

import lombok.Getter;
import lombok.Setter;

public class QueueMessageDtpCsm {
    @Getter
    @Setter
    private String version;
    @Getter
    @Setter
    private Long timestamp;
    @Getter
    @Setter
    private String typeQueueMessage;
    @Getter
    @Setter
    private String typeData;
    @Getter
    @Setter
    private Object data;
}
