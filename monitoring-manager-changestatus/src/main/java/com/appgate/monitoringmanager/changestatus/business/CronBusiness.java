package com.appgate.monitoringmanager.changestatus.business;

import com.appgate.monitoringmanager.changestatus.integration.IncidentsApi;
import com.appgate.monitoringmanager.common.enums.MonitoringStatus;
import com.appgate.monitoringmanager.persistence.dao.MonitoringConfigDao;
import com.appgate.monitoringmanager.persistence.entity.MonitoringConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.List;

@Component
@Slf4j
public class CronBusiness {
    private MonitoringConfigDao monitoringConfigDao;
    private IncidentsApi incidentsApi;

    public CronBusiness(MonitoringConfigDao monitoringConfigDao, IncidentsApi incidentsApi) {
        this.monitoringConfigDao = monitoringConfigDao;
        this.incidentsApi = incidentsApi;
    }

    public void execute() {
        log.info(new Exception().getStackTrace()[0].getMethodName());
        List<MonitoringConfig> monitoringRecords = retrieveMonitoringRecords();
        updateIncidents(monitoringRecords);
    }

    private void updateIncidents(List<MonitoringConfig> monitoringRecords) {
        log.info(new Exception().getStackTrace()[0].getMethodName());
        monitoringRecords.forEach(record -> {
            String incidentId = record.getMonitoringId();
            String status = MonitoringStatus.RESOLVED.getStatus();
            try {
                incidentsApi.incidentUpdateByIncidentIdAndStatus(incidentId, status);
            } catch (Exception e) {
                log.error("{} fail put service in incidentApi", new Exception().getStackTrace()[0].getMethodName(), e);
            }
        });
    }

    private List<MonitoringConfig> retrieveMonitoringRecords() {
        log.info(new Exception().getStackTrace()[0].getMethodName());
        ZonedDateTime now = ZonedDateTime.now();
        long limitDate = now.toEpochSecond();
        String status = MonitoringStatus.MONITORING.getStatus();
        log.info("[status:{}][Date Less Than:{}|{}][check point changed: true]", status, now, limitDate);
        List<MonitoringConfig> records = this.monitoringConfigDao.findByMonitoringFinalDateLessThanAndStatusAndCheckPointChanged(limitDate, status, Boolean.FALSE);
        log.info("records: {}", records.size());
        return records;
    }

}
