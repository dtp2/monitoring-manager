package com.appgate.monitoringmanager.changestatus.integration;

public interface IncidentsApi {

    void incidentUpdateByIncidentIdAndStatus(String incidentId, String status);
}
