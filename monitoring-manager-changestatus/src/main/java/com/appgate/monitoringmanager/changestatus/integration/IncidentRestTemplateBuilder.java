package com.appgate.monitoringmanager.changestatus.integration;

import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class IncidentRestTemplateBuilder {
    private Integer timeout;

    public IncidentRestTemplateBuilder() {
        this.timeout = 7000;
    }

    public RestTemplate buildRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        SimpleClientHttpRequestFactory rf =
                (SimpleClientHttpRequestFactory) restTemplate.getRequestFactory();
        rf.setReadTimeout(timeout);
        rf.setConnectTimeout(timeout);
        return restTemplate;
    }
}
