package com.appgate.monitoringmanager.changestatus.integration.impl;

import com.appgate.monitoringmanager.changestatus.integration.IncidentRestTemplateBuilder;
import com.appgate.monitoringmanager.changestatus.integration.IncidentsApi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.LinkedHashMap;

@Service
@Slf4j
public class IncidentsApiImpl implements IncidentsApi {
    private IncidentRestTemplateBuilder incidentRestTemplateBuilder;
    @Value("${incidentApi.url}")
    private String incidentApiUrl;

    public IncidentsApiImpl(IncidentRestTemplateBuilder incidentRestTemplateBuilder) {
        this.incidentRestTemplateBuilder = incidentRestTemplateBuilder;
    }

    @Override
    public void incidentUpdateByIncidentIdAndStatus(String incidentId, String status) {
        log.info("{} [incidentId: {}][status: {}]", new Exception().getStackTrace()[0].getMethodName(), incidentId, status);
        RestTemplate restTemplate = this.incidentRestTemplateBuilder.buildRestTemplate();
        HashMap<String, String> uriVariables = new LinkedHashMap<>();
        uriVariables.put("incidentId", incidentId);
        uriVariables.put("status", status);
        restTemplate.put(this.incidentApiUrl, null, uriVariables);
        log.info("{} put service executed ok...", new Exception().getStackTrace()[0].getMethodName());
    }
}
