package com.appgate.monitoringmanager.changestatus.entrypoint;

import com.appgate.monitoringmanager.changestatus.business.CronBusiness;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;

@Component
@Slf4j
/**
 * @see https://www.baeldung.com/cron-expressions
 */
public class CronEntryPoint {
    private CronBusiness cronBusiness;

    public CronEntryPoint(CronBusiness cronBusiness) {
        this.cronBusiness = cronBusiness;
    }

    @Scheduled(cron = "${cronExpression}")
    public void execute() {
        log.info("Cron Task :: Execution Time - {}", ZonedDateTime.now());
        try {
            cronBusiness.execute();
        } catch (Exception e) {
            log.error("Uncontrolled error", e);
        }

    }
}
