package com.appgate.monitoringmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@ComponentScan(basePackages = "com.appgate.monitoringmanager")
@EnableScheduling
public class MonitoringManagerChangestatusApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonitoringManagerChangestatusApplication.class, args);
	}

}
