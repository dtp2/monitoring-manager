package com.appgate.monitoringmanager.persistence.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "monitoring_company")
public class MonitoringCompany {

    @Id
    @Column(name = "id")
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    @Column(name = "uuid", nullable = false)
    private String uuid;

    @Getter
    @Setter
    @Column(name = "name", nullable = false)
    private String name;

    @Getter
    @Setter
    @Column(name = "countryCode", nullable = false)
    private String countryCode;

    @Getter
    @Setter
    @Column(name = "enabled", nullable = false)
    private Boolean enabled;
}
