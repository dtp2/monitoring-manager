package com.appgate.monitoringmanager.persistence.entity;

import com.appgate.monitoringmanager.common.dto.ExtraData;
import com.appgate.monitoringmanager.persistence.converters.JpaBooleanConverterJson;
import com.appgate.monitoringmanager.persistence.converters.JpaExtraDataConverterJson;
import lombok.Getter;
import lombok.Setter;


import javax.persistence.*;

@Entity
@Table(name = "monitoring_config")

public class MonitoringConfig {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    @Column(name = "monitoring_id", nullable = false)
    private String monitoringId;

    @Getter
    @Setter
    @Column(name = "url", nullable = false)
    private String url;

    @Getter
    @Setter
    @Column(name = "monitoring_final_date", nullable = false)
    private Long monitoringFinalDate;

    @Getter
    @Setter
    @Column(name = "frequency", nullable = false)
    private Integer frequency;

    @Getter
    @Setter
    @Column(name = "check_point_changed")
    @Convert(converter = JpaBooleanConverterJson.class)
    private Boolean checkPointChanged;

    @Getter
    @Setter
    @Column(name = "source_type", nullable = false)
    private String sourceType;

    @Getter
    @Setter
    @Column(name = "extra_data")
    @Convert(converter = JpaExtraDataConverterJson.class)
    private ExtraData extraData;

    @Getter
    @Setter
    @Column(name = "status", nullable = false)
    private String status;
}
