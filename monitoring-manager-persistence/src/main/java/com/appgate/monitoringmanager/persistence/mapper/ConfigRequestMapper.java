package com.appgate.monitoringmanager.persistence.mapper;

import com.appgate.monitoringmanager.common.dto.ExtraData;
import com.appgate.monitoringmanager.common.dto.MonitoringRequest;
import com.appgate.monitoringmanager.persistence.entity.MonitoringConfig;

public class ConfigRequestMapper {

    private static final String INCIDENT_ID = "incidentId";
    private static final String SOURCE_TYPE = "sourceType";
    private static final String URL = "url";

    private ConfigRequestMapper() {
    }

    public static MonitoringRequest mapper(MonitoringConfig monitoringConfig) {
        MonitoringRequest monitoringRequest = new MonitoringRequest();
        monitoringRequest.put(INCIDENT_ID, monitoringConfig.getMonitoringId());
        monitoringRequest.put(SOURCE_TYPE, monitoringConfig.getSourceType());
        monitoringRequest.put(URL, monitoringConfig.getUrl());

        for (String key : monitoringConfig.getExtraData().keySet()) {
            monitoringRequest.put(key, monitoringConfig.getExtraData().get(key));
        }
        return monitoringRequest;
    }

    public static MonitoringConfig mapper(MonitoringRequest monitoringRequest) {
        MonitoringConfig monitoringConfig = new MonitoringConfig();
        monitoringConfig.setExtraData(new ExtraData());

        for (String key : monitoringRequest.keySet()) {
            if (URL.equalsIgnoreCase(key) || INCIDENT_ID.equalsIgnoreCase(key) || SOURCE_TYPE.equalsIgnoreCase(key)) {
                monitoringConfig.setUrl((String) monitoringRequest.get(URL));
                monitoringConfig.setMonitoringId((String) monitoringRequest.get(INCIDENT_ID));
                monitoringConfig.setSourceType((String) monitoringRequest.get(SOURCE_TYPE));
            } else {
                monitoringConfig.getExtraData().put(key, monitoringConfig.getExtraData().get(key));
            }
        }
        return monitoringConfig;
    }
}
