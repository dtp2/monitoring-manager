package com.appgate.monitoringmanager.persistence.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class JpaBooleanConverterJson implements AttributeConverter<Boolean, String> {
    @Override
    public String convertToDatabaseColumn(Boolean aBoolean) {
        return aBoolean != null && aBoolean ? "true" : "false";
    }

    @Override
    public Boolean convertToEntityAttribute(String s) {
        return "true".equalsIgnoreCase(s);
    }
}
