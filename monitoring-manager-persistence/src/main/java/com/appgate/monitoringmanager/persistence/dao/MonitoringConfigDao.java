package com.appgate.monitoringmanager.persistence.dao;

import com.appgate.monitoringmanager.persistence.entity.MonitoringConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MonitoringConfigDao extends JpaRepository<MonitoringConfig, Long> {

    MonitoringConfig findByMonitoringId(String monitoringId);
    List<MonitoringConfig> findByMonitoringFinalDateGreaterThanAndFrequencyAndStatus(long monitoringFinalDate, int frecuency, String status);
    List<MonitoringConfig> findByMonitoringFinalDateLessThanAndStatusAndCheckPointChanged(long limitDate, String status, Boolean checkPointChanged);

}
