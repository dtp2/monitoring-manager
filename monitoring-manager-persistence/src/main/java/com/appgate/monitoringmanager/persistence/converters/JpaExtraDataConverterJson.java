package com.appgate.monitoringmanager.persistence.converters;

import com.appgate.monitoringmanager.common.dto.ExtraData;
import com.appgate.monitoringmanager.common.parser.JsonParse;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class JpaExtraDataConverterJson implements AttributeConverter<ExtraData, String> {

    @Override
    public String convertToDatabaseColumn(ExtraData meta) {
        return JsonParse.parseStringData(meta);
    }

    @Override
    public ExtraData convertToEntityAttribute(String dbData) {
        return JsonParse.parseJsonToObject(dbData, ExtraData.class);
    }
}
