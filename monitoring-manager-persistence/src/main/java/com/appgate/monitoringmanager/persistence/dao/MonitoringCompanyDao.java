package com.appgate.monitoringmanager.persistence.dao;

import com.appgate.monitoringmanager.persistence.entity.MonitoringCompany;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MonitoringCompanyDao extends JpaRepository<MonitoringCompany, Long> {
}
