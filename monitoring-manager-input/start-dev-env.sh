#!/bin/sh

# run compose
echo "monitoring-manager-input : starting"
cd util-files/dev-env/
docker-compose up -d

## Waiting for services to be ready
while ! echo exit | docker exec postgres-server pg_isready -h localhost -p 5432 -U monitoring_postgres_admin; do
    >&2 echo "Waiting for postgres-server";

    dbAttemps=$((dbAttemps+1))
    if [ $dbAttemps -gt 50 ]; then
        echo "Cannot start service postgres-server";
        exit 1
    fi

    sleep 5;
done
echo "regresando a raiz del proyecto"
cd ../../../
echo "aplicando scripts flyway"
cd monitoring-manager-db

echo "create schema"
mvn flyway:migrate -P schema
echo "create tables"
mvn flyway:migrate -P migrate-scripts
echo "create topics"
docker exec -it kafka-server  kafka-topics.sh --create --topic statusChangeEvent --replication-factor 1 --partitions 1 --zookeeper zookeeper-server:2181