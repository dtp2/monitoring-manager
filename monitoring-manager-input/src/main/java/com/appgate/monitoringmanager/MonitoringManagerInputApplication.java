package com.appgate.monitoringmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonitoringManagerInputApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonitoringManagerInputApplication.class, args);
	}

}
