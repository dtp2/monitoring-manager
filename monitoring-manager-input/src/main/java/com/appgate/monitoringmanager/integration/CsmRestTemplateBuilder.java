package com.appgate.monitoringmanager.integration;

import com.appgate.monitoringmanager.exceptions.ServiceException;

public interface CsmRestTemplateBuilder {
    void getCompaniesCsm() throws ServiceException;
}
