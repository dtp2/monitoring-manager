package com.appgate.monitoringmanager.integration.Impl;

import com.appgate.monitoringmanager.common.dto.Company;
import com.appgate.monitoringmanager.common.parser.JsonParse;
import com.appgate.monitoringmanager.exceptions.ServiceException;
import com.appgate.monitoringmanager.integration.CsmRestTemplateBuilder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.LinkedHashMap;

@Service
@Slf4j
public class CsmRestTemplateBuilderImpl implements CsmRestTemplateBuilder {

    @Value("${dtp.csm.rest.url}")
    private String dtpCsmUrl;

    @Value("${dtp.csm.rest.user.id}")
    private String dtpCsmUserId;

    @Value("${dtp.csm.rest.user.name}")
    private String dtpCsmUserName;

    @Value("${dtp.csm.rest.max.offset}")
    private String dtpCsmOffset;

    @Override
    public void getCompaniesCsm() throws ServiceException {

        RestTemplate restTemplate = new RestTemplate();

        HashMap<String, String> uriVariables = new LinkedHashMap<>();
        uriVariables.put("firstResult", "0");
        uriVariables.put("maxResult", dtpCsmOffset);

        HttpEntity<String> httpEntity = new HttpEntity<>(createHeaders());

        ResponseEntity<String> responseEntity = restTemplate.exchange(dtpCsmUrl, HttpMethod.GET, httpEntity, String.class, uriVariables);

        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode root = mapper.readTree(responseEntity.getBody());
            JsonNode data = root.path("data");
            Company[] company = JsonParse.parseJsonToObject(data.toString(), Company[].class);
            log.info(data.toString());
        } catch (JsonMappingException e) {
            throw new ServiceException(e.getMessage());
        } catch (JsonProcessingException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public HttpHeaders createHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("accept", "application/json");
        httpHeaders.add("X-UserId", dtpCsmUserId);
        httpHeaders.add("X-UserName", dtpCsmUserName);
        return httpHeaders;
    }
}
