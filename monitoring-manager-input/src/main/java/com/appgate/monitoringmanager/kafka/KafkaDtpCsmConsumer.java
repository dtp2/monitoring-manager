package com.appgate.monitoringmanager.kafka;

import com.appgate.monitoringmanager.business.CsmBusiness;
import com.appgate.monitoringmanager.common.dto.Company;
import com.appgate.monitoringmanager.common.dto.QueueMessageDtpCsm;
import com.appgate.monitoringmanager.common.parser.JsonParse;
import com.appgate.monitoringmanager.exceptions.ServiceException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class KafkaDtpCsmConsumer {

    private CsmBusiness csmBusiness;

    @Autowired
    public KafkaDtpCsmConsumer(CsmBusiness csmBusiness) {
        this.csmBusiness = csmBusiness;
    }

    @KafkaListener(topics = "${dtp.csm.kafka.topic}", groupId = "${dtp.csm.kafka.groupid}")
    public void listenIncidentsForValidate(String message) {
        log.info("Body input csm: {}", message);
        QueueMessageDtpCsm queueMessageDtpCsm = JsonParse.parseJsonToObject(message, QueueMessageDtpCsm.class);

        switch (queueMessageDtpCsm.getTypeData()) {
            case "Company":
                try {
                    Company company = JsonParse.parseJsonToObject(JsonParse.parseObjectToJson(queueMessageDtpCsm.getData()), Company.class);
                    csmBusiness.saveCsmCompany(company);
                } catch (ServiceException e) {
                    log.error(e.getMessage());
                }
                break;
        }
    }
}
