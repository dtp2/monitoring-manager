package com.appgate.monitoringmanager.kafka;

import com.appgate.monitoringmanager.business.IncidentBusiness;
import com.appgate.monitoringmanager.common.dto.InputIncidentData;
import com.appgate.monitoringmanager.common.enums.MonitoringStatus;
import com.appgate.monitoringmanager.common.parser.JsonParse;
import com.appgate.monitoringmanager.exceptions.ServiceException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class KafkaDtpConsumer {

    private IncidentBusiness incidentBusiness;

    @Autowired
    public KafkaDtpConsumer(IncidentBusiness incidentBusiness) {
        this.incidentBusiness = incidentBusiness;
    }

    @KafkaListener(topics = "${dtp.monitoring.manager.kafka.topic}", groupId = "${dtp.monitoring.manager.kafka.groupid}")
    public void listenIncidentsForValidate(String message) {
        log.info("Body input: {}", message);
        InputIncidentData inputIncidentData = JsonParse.parseJsonToObject(message, InputIncidentData.class);

        try {
            if (inputIncidentData.getStatus().toUpperCase().equals(MonitoringStatus.MONITORING.getStatus())) {
                incidentBusiness.saveInputIncidentData(inputIncidentData);
            } else if (inputIncidentData.getStatus().toUpperCase().equals(MonitoringStatus.RESOLVED.getStatus())) {
                incidentBusiness.findInputIncidentDataDelete(inputIncidentData);
            }
        } catch (ServiceException e) {
            log.error(e.getMessage(), e);
        }

    }
}
