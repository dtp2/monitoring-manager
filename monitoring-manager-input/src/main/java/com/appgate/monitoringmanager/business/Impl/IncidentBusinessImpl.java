package com.appgate.monitoringmanager.business.Impl;

import com.appgate.monitoringmanager.business.IncidentBusiness;
import com.appgate.monitoringmanager.common.dto.ExtraData;
import com.appgate.monitoringmanager.common.dto.InputIncidentData;
import com.appgate.monitoringmanager.exceptions.ServiceException;
import com.appgate.monitoringmanager.persistence.dao.MonitoringCompanyDao;
import com.appgate.monitoringmanager.persistence.dao.MonitoringConfigDao;
import com.appgate.monitoringmanager.persistence.entity.MonitoringCompany;
import com.appgate.monitoringmanager.persistence.entity.MonitoringConfig;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
@Log4j2
public class IncidentBusinessImpl implements IncidentBusiness {

    private MonitoringConfigDao monitoringConfigDao;

    private MonitoringCompanyDao monitoringCompanyDao;

    public IncidentBusinessImpl(MonitoringConfigDao monitoringConfigDao, MonitoringCompanyDao monitoringCompanyDao) {
        this.monitoringConfigDao = monitoringConfigDao;
        this.monitoringCompanyDao = monitoringCompanyDao;
    }

    @Override
    public void saveInputIncidentData(InputIncidentData inputIncidentData) throws ServiceException {
        Long longMonitoringFinalDate, companyIdLong;
        MonitoringCompany monitoringCompany;

        if (inputIncidentData.getUrl() == null)
            throw new ServiceException("Url empty or null");

        if (inputIncidentData.getMonitoringId() == null)
            throw new ServiceException("Monitoring Id empty or null");

        try {
            longMonitoringFinalDate = Long.parseLong(inputIncidentData.getMonitoringFinalDate());
            companyIdLong = new Long(inputIncidentData.getCompanyId());
        } catch (NumberFormatException e) {
            throw new ServiceException("Damage parse date Long");
        }

        try {
            monitoringCompany = monitoringCompanyDao.getOne(companyIdLong);
        } catch (EntityNotFoundException e) {
            throw new ServiceException(e.getMessage());
        }

        MonitoringConfig monitoringConfigExists = monitoringConfigDao.findByMonitoringId(inputIncidentData.getMonitoringId());

        if (monitoringConfigExists == null) {
            ExtraData extraData = new ExtraData();
            extraData.put("company", monitoringCompany.getName());
            extraData.put("companyId", monitoringCompany.getId());
            extraData.put("country", monitoringCompany.getCountryCode());

            MonitoringConfig monitoringConfig = new MonitoringConfig();
            monitoringConfig.setMonitoringId(inputIncidentData.getMonitoringId());
            monitoringConfig.setFrequency(inputIncidentData.getMonitoringFrecuency());
            monitoringConfig.setSourceType(inputIncidentData.getSourceType());
            monitoringConfig.setStatus(inputIncidentData.getStatus());
            monitoringConfig.setUrl(inputIncidentData.getUrl());
            monitoringConfig.setCheckPointChanged(false);
            monitoringConfig.setExtraData(extraData);
            monitoringConfig.setMonitoringFinalDate(longMonitoringFinalDate);
            monitoringConfigDao.save(monitoringConfig);
        } else {
            updateInputIncidentData(inputIncidentData, monitoringConfigExists);
        }
    }

    @Override
    public void findInputIncidentDataDelete(InputIncidentData inputIncidentData) throws ServiceException {
        try {
            MonitoringConfig monitoringConfig = monitoringConfigDao.findByMonitoringId(inputIncidentData.getMonitoringId());
            monitoringConfigDao.delete(monitoringConfig);
        } catch (Exception e) {
            throw new ServiceException("Error looking for the monitor");
        }
    }

    @Override
    public void updateInputIncidentData(InputIncidentData inputIncidentData, MonitoringConfig monitoringConfig) throws ServiceException {
        Long longMonitoringFinalDate;

        try {
            longMonitoringFinalDate = Long.parseLong(inputIncidentData.getMonitoringFinalDate());
        } catch (NumberFormatException e) {
            throw new ServiceException("Damage parse date Long");
        }

        if (longMonitoringFinalDate != monitoringConfig.getMonitoringFinalDate() || inputIncidentData.getMonitoringFrecuency() != monitoringConfig.getFrequency()) {
            monitoringConfig.setFrequency(inputIncidentData.getMonitoringFrecuency());
            monitoringConfig.setMonitoringFinalDate(longMonitoringFinalDate);
            monitoringConfigDao.save(monitoringConfig);
        }
    }


}
