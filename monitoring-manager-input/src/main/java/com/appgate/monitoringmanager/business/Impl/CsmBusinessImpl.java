package com.appgate.monitoringmanager.business.Impl;

import com.appgate.monitoringmanager.business.CsmBusiness;
import com.appgate.monitoringmanager.common.dto.Company;
import com.appgate.monitoringmanager.exceptions.ServiceException;
import com.appgate.monitoringmanager.persistence.dao.MonitoringCompanyDao;
import com.appgate.monitoringmanager.persistence.entity.MonitoringCompany;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CsmBusinessImpl implements CsmBusiness {

    private MonitoringCompanyDao monitoringCompanyDao;

    public CsmBusinessImpl(MonitoringCompanyDao monitoringCompanyDao) {
        this.monitoringCompanyDao = monitoringCompanyDao;
    }

    @Override
    public void saveCsmCompany(Company company) throws ServiceException {
        MonitoringCompany monitoringCompany=new MonitoringCompany();
        monitoringCompany.setId(company.getId());
        monitoringCompany.setCountryCode(company.getCountry().getCode());
        monitoringCompany.setName(company.getName());
        monitoringCompany.setUuid(company.getUid());
        monitoringCompany.setEnabled(company.getEnabled());
        monitoringCompanyDao.save(monitoringCompany);
    }

    @Override
    public MonitoringCompany findCompanyById(Long id) throws ServiceException {
        MonitoringCompany monitoringCompany = monitoringCompanyDao.getOne(id);
        return monitoringCompany;
    }
}
