package com.appgate.monitoringmanager.business;

import com.appgate.monitoringmanager.common.dto.Company;
import com.appgate.monitoringmanager.exceptions.ServiceException;
import com.appgate.monitoringmanager.persistence.entity.MonitoringCompany;

public interface CsmBusiness {
    void saveCsmCompany(Company company) throws ServiceException;
    MonitoringCompany findCompanyById(Long id) throws ServiceException;
}
