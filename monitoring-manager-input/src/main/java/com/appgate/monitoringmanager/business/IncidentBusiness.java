package com.appgate.monitoringmanager.business;

import com.appgate.monitoringmanager.common.dto.InputIncidentData;
import com.appgate.monitoringmanager.exceptions.ServiceException;
import com.appgate.monitoringmanager.persistence.entity.MonitoringConfig;

public interface IncidentBusiness {
    void saveInputIncidentData(InputIncidentData inputIncidentData) throws ServiceException;
    void findInputIncidentDataDelete(InputIncidentData inputIncidentData) throws ServiceException;
    void updateInputIncidentData(InputIncidentData inputIncidentData, MonitoringConfig monitoringConfig) throws ServiceException;
}
